Build Charts Application is able to create different type of Charts, like: Line, Subplot, Barh, Bar, Area, Pie, Histogram and Scatter plot. 
User can define the way, how he/she want to build charts:
    1. create plots with randomly defined numpy arrays
    2. enter values and create wanted plots

# App:
![aaa](/uploads/c4a7e33e1c91d897b86ea03ddf364855/aaa.jpg)
![bbb](/uploads/cf54d6e5f279f9844f2576db980e6c2b/bbb.jpg)
![ccc](/uploads/ad316f8dcc684e8e555833d3bf8cbcc9/ccc.jpg)
![ddd](/uploads/8d00d04939e5b2ce0715cd88e925ec40/ddd.jpg)
![eee](/uploads/339d398c98c847ed816fd2a9011f7c88/eee.jpg)
